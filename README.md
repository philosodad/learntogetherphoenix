#Learning the Phoenix Framework Together

This is a different kind of technical book.

Most technical books are written by people who have some idea of what they are doing. This book is being written by someone who doesn't know the subject, as they try to figure it out.
